import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'env.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  bool isLoading = true;
  String yourCity;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<LocationData> getLocation() async {

    //get GEO Permissionvar currentLocation 
    LocationData currentLocation;

    var location = new Location();

    try {
      currentLocation = await location.getLocation();
    } on PlatformException catch (e){
      throw Exception(e);
    }

    return currentLocation;

  }

  Future<String> getLocationCityName(LocationData locationData) async {
    
    var response = await http.get("https://geocode.xyz/${locationData.latitude.toString()},${locationData.longitude.toString()}?geoit=json&auth=$AuthKey");
    var convertedResponce = json.decode(response.body);
    return convertedResponce['city'];


  }

  showCityName(String city){
    setState(() {
      isLoading = false;
      yourCity = city;

    });
  }

  @override
  void initState() {

    getLocation()
    .then((currentLocation)=>getLocationCityName(currentLocation))
    .then((city)=>showCityName(city));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    var loading = Center(
      child: CircularProgressIndicator(),
    );
    var Content = Center(
      child: Text('Your city = $yourCity'),
    );


    return Scaffold(
      key: _scaffoldKey,
      body: isLoading==true?loading:Content,
    );
  }
}

class Geo {}
